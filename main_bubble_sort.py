import numpy as np
import statistics as stat
from scipy.optimize import curve_fit
import random
import timeit
import matplotlib.pyplot as plt
from bubble_sort import bubble


fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.set_xlabel('length of list for sorting, lg(n)')
ax.set_ylabel('spent time')

xdata = range(100, 1100, 100)
b_mean = []
s_mean = []

for n in range(100, 1100, 100):
    time_b = []
    time_s = []
    for i in range(10):
        random_list = random.sample(range(0, n), n)

        t0 = timeit.default_timer()
        bubble(random_list)
        time_b.append(timeit.default_timer() - t0)

        t0 = timeit.default_timer()
        sorted(random_list)
        time_s.append(timeit.default_timer() - t0)

    b_mean.append(stat.mean(time_b))
    s_mean.append(stat.mean(time_s))

    ax.errorbar(np.log10(n), stat.mean(time_b), yerr=stat.stdev(time_b), marker='o', color='tab:green', fmt='-o')
    ax.errorbar(np.log10(n), stat.mean(time_s), yerr=stat.stdev(time_s), marker='o', color='tab:orange', fmt='-o')

def func_sq(x, a, b):
    return a * np.power(x, 2) + b

def func_lg(x, a, b):
    return a * np.log10(x) + b


popt_b, pcov_b = curve_fit(func_sq, xdata, b_mean)
popt_s, pcov_s = curve_fit(func_lg, xdata, s_mean)

b_legend = f"a = {round(popt_b[0],9)}, b = {round(popt_b[1],9)}"
s_legend = f"a = {round(popt_s[0],9)}, b = {round(popt_s[1],9)}"
plt.plot(np.log10(xdata), func_sq(xdata, *popt_b), linestyle='dotted', color='tab:green', label=b_legend)
plt.plot(np.log10(xdata), func_lg(xdata, *popt_s), linestyle='dotted', color='tab:orange', label=s_legend)

ax.errorbar(2, 0, 0, marker='o', color='tab:green', fmt='-o', label='by "bubble sort"')
ax.errorbar(2, 0, 0, marker='o', color='tab:orange', fmt='-o', label='by "sorted"')
ax.legend(loc='upper left')
plt.show()
