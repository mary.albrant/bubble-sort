def bubble(list):
    """ this function implements bubble sorting of the list """
    n = len(list)
    for i in range(n):
        for j in range(n-i-1):
            if (list[j] > list[j+1]):
                list[j+1], list[j] = list[j], list[j+1]
    return (list)
